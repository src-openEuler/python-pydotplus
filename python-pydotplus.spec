Name:           python-pydotplus
Version:        2.0.2
Release:        11
Summary:        Python interface to Graphviz's Dot language
License:        MIT
URL:            https://pypi.python.org/pypi/pydotplus
Source0:        https://pypi.python.org/packages/source/p/pydotplus/pydotplus-2.0.2.tar.gz

BuildArch:      noarch
BuildRequires:  graphviz

%description
PyDotPlus is an improved version of the old pydot project that
provides a Python Interface to Graphviz's Dot language.

%package -n     python3-pydotplus
Summary:        Python interface to Graphviz's Dot language
%{?python_provide:%python_provide python3-pydotplus}
BuildRequires:  python3-devel python3-setuptools python3-pyparsing
Requires:       python3-pyparsing graphviz

%description -n python3-pydotplus
PyDotPlus is an improved version of the old pydot project that
provides a Python Interface to Graphviz's Dot language.

Python 3 version.

%prep
%autosetup -n pydotplus-%{version} -p1
rm -rf lib/*.egg-info

%build
%py3_build

%install
%py3_install

%check
cd test
  PYTHONPATH=%{buildroot}%{python3_sitelib} %{__python3} pydot_unittest.py -v || :

%files -n python3-pydotplus
%doc README.rst LICENSE
%{python3_sitelib}/pydotplus*

%changelog
* Fri Jul 14 2023 wangxiyuan <wangxiyuan1007@gmail.com> - 2.0.2-11
- Drop python2 support
* Tue Mar 10 2020 zhangtao <zhangtao221@huawei.com> - 2.0.2-10
- package init
